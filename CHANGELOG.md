# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/pyspc/pyspc-formation/compare/v1.0.0...v1.0.1) (2020-02-05)


### Bug Fixes

* **typos:** suppression boulettes ([3f4d308](https://gitlab.com/pyspc/pyspc-formation/commit/3f4d3081ddc60a1959ad33a4bad90ca74589b117))

## [1.0.0](https://gitlab.com/pyspc/pyspc-formation/compare/v0.1.1...v1.0.0) (2020-02-03)


### Features

* **notebooks:** add notebooks prez ([444f55f](https://gitlab.com/pyspc/pyspc-formation/commit/444f55fcfebde66520be8819c69193eac50a08be))
* **site:** make site fully static ([5fed9c3](https://gitlab.com/pyspc/pyspc-formation/commit/5fed9c310fed76360e55360562cd3b2b117e9ab7))
* **struct:** nouvelle organisation ([cf427e7](https://gitlab.com/pyspc/pyspc-formation/commit/cf427e7b7b9921a5e8879bc522a15769d2f4ba82))
* **struct:** nouvelle organisation ([02326ac](https://gitlab.com/pyspc/pyspc-formation/commit/02326acde10014a4ddbf7076a6bc0745d967f92d))


### Bug Fixes

* **misc:** typos ([9a40770](https://gitlab.com/pyspc/pyspc-formation/commit/9a40770b25abf7c77c1139f2a64b71b5e721cef0))
* **reqs:** Fix typo in requirements ([8513c6a](https://gitlab.com/pyspc/pyspc-formation/commit/8513c6a03c9306cdad021fe29e6b2a5318407406))

## [0.1.1](https://gitlab.com/pyspc/pyspc-formation/compare/v0.1.0...v0.1.1) (2019-06-12)


### Bug Fixes

* **docs:** derniers reglages ([d89d704](https://gitlab.com/pyspc/pyspc-formation/commit/d89d704))
* **pdf:** update pdsfs ([b850188](https://gitlab.com/pyspc/pyspc-formation/commit/b850188))



# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# [0.1.0](https://gitlab.com/pyspc/pyspc-formation/compare/v0.0.2...v0.1.0) (2019-06-12)


### Bug Fixes

* **docs:** fix download links ([7d4d9d2](https://gitlab.com/pyspc/pyspc-formation/commit/7d4d9d2))
* **docs:** modifications cosmétiques ([0527412](https://gitlab.com/pyspc/pyspc-formation/commit/0527412))


### Features

* **doc:** liens de téléchargement / run / pdf ([040323d](https://gitlab.com/pyspc/pyspc-formation/commit/040323d))



# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 0.0.2 (2019-06-11)
