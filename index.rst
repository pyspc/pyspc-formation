Python pour la SPC au lycée - formation
#######################################



Déroulement
-----------

Télécharger le :download:`document PDF correspondant <intro/organisation-formation.pdf>`

- Présentation des objectifs de la formation - :download:`intro rapide <intro/intro.pdf>`.
- Présentation de l'outil **notebook jupyter** et introduction rapide au python
- Mise en activité "multi-entrées" :
  
  - progression différenciée proposée ci-dessous
  - autonomie à l'aide du site https://pyspc.readthedocs.io
  - autonomie totale - réalisation de programmes en lien avec les nouveaux programmes

- Petite pratique "à la maison" entre les deux sessions


Progression
-----------

.. toctree::
   :maxdepth: 1
   
   notions/bases.ipynb
   etape_01/dissolution_prof.ipynb
   etape_02/structure_atome-episode_1.ipynb
   etape_03/structure_atome-episode_2.ipynb
   etape_04/ohm_episode_1.ipynb
   etape_05/ohm_episode_2.ipynb
   etape_06/mvt_parabolique_coord_vect_vitesse.ipynb
   etape_07/mvt_parabolique_trace_vect_vitesse.ipynb
   etape_08/mvt_parabolique_import_donnees.ipynb
   etape_08/chute_balle_cycliste/chute_balle_cycliste.ipynb
   etape_09/suivi_avancement_eleve.ipynb
   etape_10/signal_periodique_eleves.ipynb
   etape_11/mouvement_satellite_geostationnaire_eleve.ipynb
   etape_12/animation_onde_corde_long_a_charger.ipynb
   etape_13/index.rst
   notions/reinvestissement_J1.ipynb


Accès et téléchargements
========================

Accès
-----

Ce contenu de formation est accessible parallèlement au guide `Python pour la SPC au lycée <https://pyspc.readthedocs.io>`_ :

- en ligne via `ReadTheDocs <https://pyspc-formation.readthedocs.io/fr/latest/index.html#>`_
- dans une archive `ZIP à télécharger <https://gitlab.com/pyspc/pyspc-formation/-/archive/master/pyspc-formation-master.zip>`_
- au format `PDF à télécharger <https://buildmedia.readthedocs.org/media/pdf/pyspc-formation/latest/pyspc-formation.pdf>`_
- au format `Epub à télécharger <https://readthedocs.org/projects/pyspc-formation/downloads/epub/latest/>`_
- en mode démo exécutable via `binder <https://mybinder.org/v2/gl/pyspc%2Fpyspc-formation/master>`_ (le chargement initial peut être long)
- sur `gitlab <https://gitlab.com/pyspc/pyspc-formation>`_

Licence
-------

.. image:: _static/ccbysa.png

`Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International* <https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr>`_

**Auteurs, par ordre alphabétique**

- BARBIER Jean-Matthieu <jean-matthieu.barbier@ac-rouen.fr>
- DENIS Cyril <cyril.denis@ac-rouen.fr>
- DEVEDEUX Dominique <dominique.devedeux@ac-rouen.fr>
- DENDIEVEL Alexis <alexis.dendievel@ac-rouen.fr>
- REBOLINI Gaelle <gaelle-nathalie.rebolini@ac-rouen.fr>
