---
title: Python pour la SPC
---

# Quoi et pourquoi

## Python pour la SPC

**Apparition dans le BO (nouveaux programmes 2019)**

- graphiques
- modélisations
- animations

16 mentions : 

- 4 en classe de seconde
- 4 en classe de 1ère
- 8 en classe de terminale

## Python

- langage interprété
- créé vers 1980
- utilisé pour l'enseignement
- utilisé dans le monde pro


## Python et les élèves

Arrivée de la programmation au programme du lycée via **python**

- en maths
- en SNT
- en SPC 
- en NSI


# Python pour la SPC

## Objectifs

**pas une formation généraliste python**

- utiliser
- comprendre
- modifier
- créer


## Ressources

Deux sites disponibles :

- Formation : **https://pyspc-formation.readthedocs.io** ou lien académique

- Référence : **https://pyspc.readthedocs.io** ou lien académique


## Modes de formation

- guidé via site formation
- en autonomie via site formation
- en autonomie via site référence 
- en autonomie totale
